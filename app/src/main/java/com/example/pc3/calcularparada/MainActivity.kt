package com.example.pc3.calcularparada

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var mTextWtValor: TextWatcher? = null

    lateinit var adView: AdView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this, "ca-app-pub-1014232296081686~3100178665")

        fab.setOnClickListener { view ->
            edtJogo1Cota.setText("0.0")
            edtJogo2cota.setText("0.0")
            edtValor.setText("0.0")
        }

        adView = findViewById(R.id.adView)
        // Propaganda
        val adRequest = AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build()
        adView.loadAd(adRequest)


        /////////////////


        listener()
    }

    override fun onResume() {
        super.onResume()
        adView.resume()
    }

    override fun onPause() {
        adView.pause()
        super.onPause()
    }

    override fun onDestroy() {
        adView.destroy()
        super.onDestroy()
    }

    private fun listener() {

        mudancaTextoValor()
        edtValor.addTextChangedListener(mTextWtValor)
    }

    private fun mudancaTextoValor() {
        mTextWtValor = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (edtJogo1Cota.text.isNotEmpty() || edtJogo2cota.text.isNotEmpty()
                        || edtValor.text.isNotEmpty() || edtValor.text.toString().toDouble() > 0.toDouble()) {

                    try {
                        calcular(s.toString().replace(",".toRegex(), ".").toDouble(),
                                edtJogo1Cota.text.toString().replace(",".toRegex(), ".").toDouble(),
                                edtJogo2cota.text.toString().replace(",".toRegex(), ".").toDouble())
                    } catch (e: Exception) {
                        Toast.makeText(this@MainActivity, e.message.toString(), Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }


    private fun calcular(valor: Double, cota1: Double, cota2: Double) {
        val tx = valor / (cota1 + cota2)
        var valor1 = tx * cota1
        var valor2 = tx * cota2

        if (valor1.isNaN()) {
            valor1 = 0.0
        }
        if (valor2.isNaN()) {
            valor2 = 0.0
        }
        edtJogo1Valor.setText(String.format("%.2f", valor2))
        edtJogo2Valor.setText(String.format("%.2f", valor1))
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
